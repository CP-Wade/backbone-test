<?php

use Illuminate\Database\Migrations\Migration;

class SetupCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

		Schema::create('companies', function($t){
			$t->increments('id');
			$t->timestamps();
			$t->string('name');
			$t->string('firm_type');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

		Schema::drop('companies');
	}

}