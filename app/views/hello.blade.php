<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script src="{{ asset( 'assets/js/jquery.js' ) }}"></script>
	<script src="http://ajax.cdnjs.com/ajax/libs/json2/20110223/json2.js"></script>
	<script src="{{ asset('assets/js/backbone.js') }}"></script>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
</head>
<body>
<div class="container">

</div>
<script type="text/template" id="company-template">
    <% _.each(companies, function(company) { %>
	    <div class="company">
	        <p>Name: <%= company.name %></p>
            <p>Type: <%= company.firm_type %></p>
	    </div>
    <% }); %>
</script>
<script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
</body>
</html>