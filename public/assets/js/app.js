(function ($) {

    var Company = Backbone.Model.extend();

    var Companies = Backbone.Collection.extend({
        model: Company,
        url: '/api/v1/company'
    });

    var AppView = Backbone.View.extend({
        el: $('.container'),
        template: _.template($('#company-template').html()),
        model: Company,
        collection: new Companies,

        initialize: function(){
            _.bindAll(this, 'render');
            var self = this;

            this.collection.fetch({success: function(){ self.render() }});
        },

        render: function(){

            this.$el.html(this.template({companies: this.collection.toJSON() }));
            return this;

        }

    });

    new AppView;

})(jQuery);